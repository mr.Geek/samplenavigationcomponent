package com.hachdev.samplenavigationcomponent.fragments

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.Int
import kotlin.String
import kotlin.jvm.JvmStatic

data class PageFragmentArgs(val pageNumber: Int, val pageParent: String) : NavArgs {
    fun toBundle(): Bundle {
        val result = Bundle()
        result.putInt("pageNumber", this.pageNumber)
        result.putString("pageParent", this.pageParent)
        return result
    }

    companion object {
        @JvmStatic
        fun fromBundle(bundle: Bundle): PageFragmentArgs {
            bundle.setClassLoader(PageFragmentArgs::class.java.classLoader)
            val __pageNumber : Int
            if (bundle.containsKey("pageNumber")) {
                __pageNumber = bundle.getInt("pageNumber")
            } else {
                throw IllegalArgumentException("Required argument \"pageNumber\" is missing and does not have an android:defaultValue")
            }
            val __pageParent : String?
            if (bundle.containsKey("pageParent")) {
                __pageParent = bundle.getString("pageParent")
                if (__pageParent == null) {
                    throw IllegalArgumentException("Argument \"pageParent\" is marked as non-null but was passed a null value.")
                }
            } else {
                throw IllegalArgumentException("Required argument \"pageParent\" is missing and does not have an android:defaultValue")
            }
            return PageFragmentArgs(__pageNumber, __pageParent)
        }
    }
}
