package com.hachdev.samplenavigationcomponent

import android.os.Bundle
import androidx.navigation.NavDirections
import kotlin.Int
import kotlin.String

class NavigationGraphMainDirections private constructor() {
    private data class ActionGlobalPageFragment(val pageNumber: Int, val pageParent: String) :
            NavDirections {
        override fun getActionId(): Int = R.id.action_global_pageFragment

        override fun getArguments(): Bundle {
            val result = Bundle()
            result.putInt("pageNumber", this.pageNumber)
            result.putString("pageParent", this.pageParent)
            return result
        }
    }

    companion object {
        fun actionGlobalPageFragment(pageNumber: Int, pageParent: String): NavDirections =
                ActionGlobalPageFragment(pageNumber, pageParent)
    }
}
