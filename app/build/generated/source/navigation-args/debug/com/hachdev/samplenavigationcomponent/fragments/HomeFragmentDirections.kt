package com.hachdev.samplenavigationcomponent.fragments

import androidx.navigation.NavDirections
import com.hachdev.samplenavigationcomponent.NavigationGraphMainDirections
import kotlin.Int
import kotlin.String

class HomeFragmentDirections private constructor() {
    companion object {
        fun actionGlobalPageFragment(pageNumber: Int, pageParent: String): NavDirections =
                NavigationGraphMainDirections.actionGlobalPageFragment(pageNumber, pageParent)
    }
}
